<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8cfec70077c1d8240516b90128ea4b12a0778650',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8cfec70077c1d8240516b90128ea4b12a0778650',
            'dev_requirement' => false,
        ),
        'bramus/router' => array(
            'pretty_version' => '1.6.1',
            'version' => '1.6.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bramus/router',
            'aliases' => array(),
            'reference' => '55657b76da8a0a509250fb55b9dd24e1aa237eba',
            'dev_requirement' => false,
        ),
    ),
);
