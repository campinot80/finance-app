<? require(__DIR__ . "/router.php")?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Finance App</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <?php require("view/navbar.php") ?>
        <script src="" async defer></script>
    </body>
</html>