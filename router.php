<?
 require (__DIR__ . "/vendor/autoload.php");

$router = new \Bramus\Router\Router();

if ($env['debug'] and preg_match('/\.(?:png|jpg|jpeg|css|js|svg)$/', $_SERVER["REQUEST_URI"])) {
    return false;
}

$router->get('/login', new User($_POST['name'], $_POST['password']));



$router->run();